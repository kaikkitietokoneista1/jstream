const app = require('express')();
const http = require('http').Server(app);
const screenshot = require('screenshot-desktop');
const port = 8080;

app.get('/', function(req, res){
  screenshot({ filename: 'screencapture.png' });
  res.sendFile(__dirname + '/index.html');
});

app.get('/screencapture.png', function(req, res){
  res.sendFile(__dirname + '/screencapture.png');
});

http.listen(port, function(){
  console.log('listening on *:' + port);
});
